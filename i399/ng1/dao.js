'use strict';

const mongodb = require('mongodb');
const ObjectID = mongodb.ObjectID;
const COLLECTION = 'contacts';



class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    addContact(contact){
        return this.db.collection(COLLECTION).insertOne(contact);
    }

    getContacts() {
        return this.db.collection(COLLECTION).find().toArray();
    }

    getContact(id) {
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).findOne({ _id: id });
    }

    updateContact(id, contact){
        contact._id = new ObjectID(id);
        return this.db.collection(COLLECTION).
        updateOne({ _id: contact._id }, contact);
    };

    deleteContact(id){
        id = new ObjectID(id);
        return this.db.collection(COLLECTION)
            .removeOne({ _id: id });
    };

    deleteContacts(ids){
        var contactIds = [];
        for(let id of ids){
            contactIds.push(new ObjectID(id));
        };
        return this.db.collection(COLLECTION)
            .removeMany({_id: {$in: contactIds}});
    };

    close() {
        if (this.db) {
            this.db.close();
        }
    }
}

module.exports = Dao;
