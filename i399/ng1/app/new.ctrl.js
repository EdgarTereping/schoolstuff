(function () {
    'use strict';

    angular.module('app').controller('NewCtrl', Ctrl);

    function Ctrl($http, $location) {
        var vm = this;
        this.newContactPhone = "";
        this.newContactName = "";
        this.addContact = addContact;

        function addContact() {
            var newContact = {
                name : vm.newContactName,
                phone : vm.newContactPhone,
                selected : false
            };

            $http.post('/api/contacts', newContact).then(back);
        }

        function back() {
            $location.path('/search');
        }

    }

})();

