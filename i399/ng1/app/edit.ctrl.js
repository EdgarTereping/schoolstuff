(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location, $scope) {
        var vm = this;
        var id = $routeParams.id;
        this.contact = {};
        this.newContactName = '';
        this.newContactPhone = '';
        this.editContact = editContact;

        init();

        function init() {
            $http.get('api/contacts/' + id).then(function(result) {
                vm.contact = result.data;
                setFieldValues();
            });
        }

        function setFieldValues(){
            // for the view
            $scope.name = vm.contact.name;
            $scope.phone = vm.contact.phone;

            // for the model
            vm.newContactName = vm.contact.name;
            vm.newContactPhone = vm.contact.phone;
        }

        function editContact() {
            vm.contact.name = vm.newContactName;
            vm.contact.phone = vm.newContactPhone;

            $http.put('api/contacts/' + id, vm.contact).then(back);
        }

        function back() {
            $location.path('/search');
        }
    }
})();