(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, modalService) {
        this.contacts = [];
        this.removeContact = removeContact;
        var vm = this;

        init();

        function init(){
            $http.get('api/contacts').then(function (result) {
                vm.contacts = result.data;
            });
        }

        function removeContact(id){
            modalService.confirm()
                .then(function () {
                    $http.delete('api/contacts/' + id);
                }).then(init);

        }

    }

})();

