'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const Dao = require('./dao.js');

const app = express();
const dao = new Dao();

const url = 'mongodb://testkasutaja:testkasutaja@ds157538.mlab.com:57538/i399';

app.use(bodyParser.json());
app.use(express.static('./'));

app.post('/api/contacts', addContact);
app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.put('/api/contacts/:id', updateContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', deleteContacts);

app.use(errorHandler); // after request handlers

dao.connect(url)
    .then(() => {
        app.listen(3000, () => console.log('Server is running on port 3000'));
    }).catch(error => {
    console.log('Error: ' + error)
});

function addContact(request, response, next) {
    dao.addContact(request.body)
        .then(() => response.end())
        .catch(next);
}

function getContacts(request, response, next) {
    dao.getContacts()
        .then(contacts => response.json(contacts))
        .catch(next);
}

function getContact(request, response, next) {
    var id = request.params.id;
    dao.getContact(id)
        .then(contact => response.json(contact))
        .catch(next);
}

function updateContact(request, response, next) {
    var id = request.params.id;
    dao.updateContact(id, request.body)
        .then(() => response.end())
        .catch(next);
}

function deleteContact(request, response, next) {
    var id = request.params.id;
    dao.deleteContact(id)
        .then(() => response.end())
        .catch(next);
}

function deleteContacts(request, response, next) {
    var ids = request.body;
    dao.deleteContacts(ids)
        .then(() => response.end())
        .catch(next);
}

function errorHandler(error, request, response, next) { // there must be 4 arguments
    response.status(500).json({ error2: error.toString() });
}