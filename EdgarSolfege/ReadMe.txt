This program plays a random interval and the user has to identify what interval it played.

The user starts by clicking the New Interval button. The program then plays two random notes with the second one being higher than the first one.
The user then has to identify what ascending interval was played by clicking on the button with the correct interval name.
The user can repeat the interval by clicking the Repeat button.
The user can also give up by clicking the Give Up button upon which the correct interval name is displayed.

author Edgar Tereping
version 1.01 15 May 2017