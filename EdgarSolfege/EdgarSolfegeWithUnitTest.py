import unittest
from random import Random
from tkinter import *
from winsound import *

userAnswer = 0
realAnswer = 0
a = 0
b = 0

random = Random()


def gen_response():
    # generates an integer between 17 and 24
    return random.randint(17, 24)


class RandomNumberTest(unittest.TestCase):

    def setUp(self):
        global random
        random = Random(123)

    def test_gen_response(self):
        self.assertEqual(gen_response(), 17)


def main():

    root = Tk()
    root.geometry("350x400")
    root.wm_title("EdgarSolfege")

    """background_image=tkinter.PhotoImage(file = 'Art/taust.gif')
    background_label = tkinter.Label(root, image=background_image)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    background_label.pack()"""

    frame6 = Frame(root)
    frame6.pack(side=BOTTOM)
    frame5 = Frame(root)
    frame5.pack(side=BOTTOM)
    frame4 = Frame(root)
    frame4.pack(side=BOTTOM)
    frame3 = Frame(root)
    frame3.pack(side=BOTTOM)
    frame2 = Frame(root)
    frame2.pack(side=BOTTOM)
    frame1 = Frame(root)
    frame1.pack(side=BOTTOM)

    Instructions = Label(root, text="Identify the ascending interval", font=("Helvetica", 16))
    Instructions.pack(pady=20)

    t = Text(frame5, height=3, width=30)
    t.tag_add('correct', '1.0')
    t.tag_add('false', '1.0')
    t.tag_add('give_up', '1.0')
    t.tag_configure('correct', foreground='green', font='helvetica 16 bold', relief='raised', justify='center',
                    spacing1=13)
    t.tag_configure('false', foreground='red', font='helvetica 16 bold', relief='raised', justify='center', spacing1=13)
    t.tag_configure('give_up', font='helvetica 16 bold', relief='raised', justify='center', spacing1=13)
    t.pack()
    

    def answer(userAnswer):
        print("userAnswer = " + str(userAnswer))
        if realAnswer == 0:
            pass
        elif userAnswer == realAnswer:
            answer = "Correct"
            t.delete('1.0', END)
            t.insert(END, answer, 'correct')
            c = random.randint(1, 8)
            playResponse(c)
        else:
            answer = "False"
            c = random.randint(9, 16)
            playResponse(c)
            t.delete('1.0', END)
            t.insert(END, answer, 'false')


    def correctAnswer():
        if realAnswer == 0:
            answer = ""
        elif realAnswer == 1:
            answer = "Minor second"
        elif realAnswer == 2:
            answer = "Major second"
        elif realAnswer == 3:
            answer = "Minor third"
        elif realAnswer == 4:
            answer = "Major third"
        elif realAnswer == 5:
            answer = "Perfect fourth"
        elif realAnswer == 6:
            answer = "Diminished fifth"
        elif realAnswer == 7:
            answer = "Perfect fifth"
        elif realAnswer == 8:
            answer = "Minor sixth"
        elif realAnswer == 9:
            answer = "Major sixth"
        elif realAnswer == 10:
            answer = "Minor seventh"
        elif realAnswer == 11:
            answer = "Major seventh"
        elif realAnswer == 12:
            answer = "Perfect octave"
        elif realAnswer == 13:
            answer = "Minor ninth"
        elif realAnswer == 14:
            answer = "Major ninth"
        elif realAnswer == 15:
            answer = "Minor decim"
        else:
            answer = "Major decim"
        t.delete('1.0', END)
        t.insert(END, answer, 'give_up')
        c = gen_response()
        playResponse(c)

    button1 = Button(frame1, text="Minor" + "\n" + "second", command = lambda: answer(1), height = 3, width = 8)
    button1.pack(side=LEFT)
    button2 = Button(frame1, text="Perfect" + "\n" + "fourth", command = lambda: answer(5), height = 3, width = 8)
    button2.pack(side=LEFT)
    button3 = Button(frame1, text="Major" + "\n" + " sixth", command = lambda: answer(9), height = 3, width = 8)
    button3.pack(side=LEFT)
    button4 = Button(frame1, text="Minor" + "\n" + " ninth", command = lambda: answer(13), height = 3, width = 8)
    button4.pack(side=LEFT)
    button5 = Button(frame2, text="Major" + "\n" + " second", command = lambda: answer(2), height = 3, width = 8)
    button5.pack(side=LEFT)
    button6 = Button(frame2, text="Diminished" + "\n" + " fifth", command = lambda: answer(6), height = 3, width = 8)
    button6.pack(side=LEFT)
    button7 = Button(frame2, text="Minor" + "\n" + " seventh", command = lambda: answer(10), height = 3, width = 8)
    button7.pack(side=LEFT)
    button8 = Button(frame2, text="Major" + "\n" + " ninth", command = lambda: answer(14), height = 3, width = 8)
    button8.pack(side=LEFT)
    button9 = Button(frame3, text="Minor" + "\n" + " third", command = lambda: answer(3), height = 3, width = 8)
    button9.pack(side=LEFT)
    button10 = Button(frame3, text="Perfect" + "\n" + " fifth", command = lambda: answer(7), height = 3, width = 8)
    button10.pack(side=LEFT)
    button11 = Button(frame3, text="Major" + "\n" + " seventh", command = lambda: answer(11), height = 3, width = 8)
    button11.pack(side=LEFT)
    button12 = Button(frame3, text="Minor" + "\n" + " decim", command = lambda: answer(15), height = 3, width = 8)
    button12.pack(side=LEFT)
    button13 = Button(frame4, text="Major" + "\n" + " third", command = lambda: answer(4), height = 3, width = 8)
    button13.pack(side=LEFT)
    button14 = Button(frame4, text="Minor" + "\n" + " sixth", command = lambda: answer(8), height = 3, width = 8)
    button14.pack(side=LEFT)
    button15 = Button(frame4, text="Perfect" + "\n" + " octave", command = lambda: answer(12), height = 3, width = 8)
    button15.pack(side=LEFT)
    button16 = Button(frame4, text="Major" + "\n" + " decim", command = lambda: answer(16), height = 3, width = 8)
    button16.pack(side=LEFT)


    def randomNumber():
        global a, b
        a = random.randint(1, 28)
        if a < 15:
            b = random.randint(a + 1, a + 15)
        else:
            b = random.randint(a + 1, 29)
        print("a = " + str(a))
        print("b = " + str(b))
        play()


    def playResponse(c):
        if c == 1:
            PlaySound("Helifailid/CorrectEnglish.wav", SND_FILENAME)
        elif c == 2:
            PlaySound("Helifailid/CorrectEstonian.wav", SND_FILENAME)
        elif c == 3:
            PlaySound("Helifailid/CorrectFinnish.wav", SND_FILENAME)
        elif c == 4:
            PlaySound("Helifailid/CorrectGerman.wav", SND_FILENAME)
        elif c == 5:
            PlaySound("Helifailid/CorrectItalian.wav", SND_FILENAME)
        elif c == 6:
            PlaySound("Helifailid/CorrectJapanese.wav", SND_FILENAME)
        elif c == 7:
            PlaySound("Helifailid/CorrectLatin.wav", SND_FILENAME)
        elif c == 8:
            PlaySound("Helifailid/CorrectRussian.wav", SND_FILENAME)
        elif c == 9:
            PlaySound("Helifailid/FalseEnglish.wav", SND_FILENAME)
        elif c == 10:
            PlaySound("Helifailid/FalseEstonian.wav", SND_FILENAME)
        elif c == 11:
            PlaySound("Helifailid/FalseFinnish.wav", SND_FILENAME)
        elif c == 12:
            PlaySound("Helifailid/FalseGerman.wav", SND_FILENAME)
        elif c == 13:
            PlaySound("Helifailid/FalseItalian.wav", SND_FILENAME)
        elif c == 14:
            PlaySound("Helifailid/FalseJapanese.wav", SND_FILENAME)
        elif c == 15:
            PlaySound("Helifailid/FalseLatin.wav", SND_FILENAME)
        elif c == 16:
            PlaySound("Helifailid/FalseRussian.wav", SND_FILENAME)
        elif c == 17:
            PlaySound("Helifailid/DisappointedEnglish.wav", SND_FILENAME)
        elif c == 18:
            PlaySound("Helifailid/DisappointedEstonian.wav", SND_FILENAME)
        elif c == 19:
            PlaySound("Helifailid/DisappointedFinnish.wav", SND_FILENAME)
        elif c == 20:
            PlaySound("Helifailid/DisappointedGerman.wav", SND_FILENAME)
        elif c == 21:
            PlaySound("Helifailid/DisappointedItalian.wav", SND_FILENAME)
        elif c == 22:
            PlaySound("Helifailid/DisappointedJapanese.wav", SND_FILENAME)
        elif c == 23:
            PlaySound("Helifailid/DisappointedLatin.wav", SND_FILENAME)
        else:
            PlaySound("Helifailid/DisappointedRussian.wav", SND_FILENAME)
        print("c = " + str(c))


    def play():
        if a == 0:
            pass
        elif a == 1:
            PlaySound("Helifailid/C2.wav", SND_FILENAME)
        elif a == 2:
            PlaySound("Helifailid/Csharp2.wav", SND_FILENAME)
        elif a == 3:
            PlaySound("Helifailid/D2.wav", SND_FILENAME)
        elif a == 4:
            PlaySound("Helifailid/Dsharp2.wav", SND_FILENAME)
        elif a == 5:
            PlaySound("Helifailid/E2.wav", SND_FILENAME)
        elif a == 6:
            PlaySound("Helifailid/F2.wav", SND_FILENAME)
        elif a == 7:
            PlaySound("Helifailid/Fsharp2.wav", SND_FILENAME)
        elif a == 8:
            PlaySound("Helifailid/G2.wav", SND_FILENAME)
        elif a == 9:
            PlaySound("Helifailid/Gsharp2.wav", SND_FILENAME)
        elif a == 10:
            PlaySound("Helifailid/A2.wav", SND_FILENAME)
        elif a == 11:
            PlaySound("Helifailid/Asharp2.wav", SND_FILENAME)
        elif a == 12:
            PlaySound("Helifailid/B2.wav", SND_FILENAME)
        elif a == 13:
            PlaySound("Helifailid/C3.wav", SND_FILENAME)
        elif a == 14:
            PlaySound("Helifailid/Csharp3.wav", SND_FILENAME)
        elif a == 15:
            PlaySound("Helifailid/D3.wav", SND_FILENAME)
        elif a == 16:
            PlaySound("Helifailid/Dsharp3.wav", SND_FILENAME)
        elif a == 17:
            PlaySound("Helifailid/E3.wav", SND_FILENAME)
        elif a == 18:
            PlaySound("Helifailid/F3.wav", SND_FILENAME)
        elif a == 19:
            PlaySound("Helifailid/Fsharp3.wav", SND_FILENAME)
        elif a == 20:
            PlaySound("Helifailid/G3.wav", SND_FILENAME)
        elif a == 21:
            PlaySound("Helifailid/Gsharp3.wav", SND_FILENAME)
        elif a == 22:
            PlaySound("Helifailid/A3.wav", SND_FILENAME)
        elif a == 23:
            PlaySound("Helifailid/Asharp3.wav", SND_FILENAME)
        elif a == 24:
            PlaySound("Helifailid/B3.wav", SND_FILENAME)
        elif a == 25:
            PlaySound("Helifailid/C4.wav", SND_FILENAME)
        elif a == 26:
            PlaySound("Helifailid/Csharp4.wav", SND_FILENAME)
        elif a == 27:
            PlaySound("Helifailid/D4.wav", SND_FILENAME)
        else:
            PlaySound("Helifailid/Dsharp4.wav", SND_FILENAME)

        if b == 0:
            pass
        elif b == 2:
            PlaySound("Helifailid/Csharp2.wav", SND_FILENAME)
        elif b == 3:
            PlaySound("Helifailid/D2.wav", SND_FILENAME)
        elif b == 4:
            PlaySound("Helifailid/Dsharp2.wav", SND_FILENAME)
        elif b == 5:
            PlaySound("Helifailid/E2.wav", SND_FILENAME)
        elif b == 6:
            PlaySound("Helifailid/F2.wav", SND_FILENAME)
        elif b == 7:
            PlaySound("Helifailid/Fsharp2.wav", SND_FILENAME)
        elif b == 8:
            PlaySound("Helifailid/G2.wav", SND_FILENAME)
        elif b == 9:
            PlaySound("Helifailid/Gsharp2.wav", SND_FILENAME)
        elif b == 10:
            PlaySound("Helifailid/A2.wav", SND_FILENAME)
        elif b == 11:
            PlaySound("Helifailid/Asharp2.wav", SND_FILENAME)
        elif b == 12:
            PlaySound("Helifailid/B2.wav", SND_FILENAME)
        elif b == 13:
            PlaySound("Helifailid/C3.wav", SND_FILENAME)
        elif b == 14:
            PlaySound("Helifailid/Csharp3.wav", SND_FILENAME)
        elif b == 15:
            PlaySound("Helifailid/D3.wav", SND_FILENAME)
        elif b == 16:
            PlaySound("Helifailid/Dsharp3.wav", SND_FILENAME)
        elif b == 17:
            PlaySound("Helifailid/E3.wav", SND_FILENAME)
        elif b == 18:
            PlaySound("Helifailid/F3.wav", SND_FILENAME)
        elif b == 19:
            PlaySound("Helifailid/Fsharp3.wav", SND_FILENAME)
        elif b == 20:
            PlaySound("Helifailid/G3.wav", SND_FILENAME)
        elif b == 21:
            PlaySound("Helifailid/Gsharp3.wav", SND_FILENAME)
        elif b == 22:
            PlaySound("Helifailid/A3.wav", SND_FILENAME)
        elif b == 23:
            PlaySound("Helifailid/Asharp3.wav", SND_FILENAME)
        elif b == 24:
            PlaySound("Helifailid/B3.wav", SND_FILENAME)
        elif b == 25:
            PlaySound("Helifailid/C4.wav", SND_FILENAME)
        elif b == 26:
            PlaySound("Helifailid/Csharp4.wav", SND_FILENAME)
        elif b == 27:
            PlaySound("Helifailid/D4.wav", SND_FILENAME)
        elif b == 28:
            PlaySound("Helifailid/Dsharp4.wav", SND_FILENAME)
        else:
            PlaySound("Helifailid/E4.wav", SND_FILENAME)
        global realAnswer
        realAnswer = b - a
        print("realAnswer = " + str(realAnswer))

    button17 = Button(frame6, text="New interval", command=randomNumber, height=2, width=10)
    button17.pack(side=LEFT)
    button18 = Button(frame6, text="Repeat", command=play, height=2, width=10)
    button18.pack(side=LEFT)
    button19 = Button(frame6, text="Give up", command=correctAnswer, height=2, width=10)
    button19.pack(side=LEFT)

    root.mainloop()

"""if __name__ == '__main__':
    main()"""
if __name__ == '__main__':
    unittest.main()
