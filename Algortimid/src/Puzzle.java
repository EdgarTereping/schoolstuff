import java.util.HashMap;
import java.util.HashSet;

public class Puzzle {

   /** Solve the word puzzle.
    * @param args three words (addend1, addend2 and sum)
    */
	// http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java
	
	private static String letters;
	private static String[] words;
	private static int answers;

	public static void main(String[] args) {
		cryptarithm(args[0], args[1], args[2]);
		answers = 0;
	}
	
	
	public static void cryptarithm(String s1, String s2, String s3) {
		 words = new String[3];
		    words[0] = s1.toUpperCase();
		    words[1] = s2.toUpperCase();
		    words[2] = s3.toUpperCase();
	    if (words[2].length() - 1 > words[0].length() && words[2].length() - 1 > words[1].length())
	    	throw new IllegalArgumentException ("No possible answers. First two strings are too short.");
	    if (words[2].length() < words[0].length() || words[2].length() < words[1].length())
	    	throw new IllegalArgumentException ("No possible answers. Result string is too short.");	   
	    String all = words[0] + words[1] + words[2];
	    letters = "";
	    for (int i = 0; i < all.length(); i++) {
	    	char c = all.charAt(i);
	    	if (letters.indexOf(c) < 0) letters += c;
	    }
	    if (letters.length() > 10)
	    	throw new IllegalArgumentException ("Too many different letters. Number of different letters must be smaller than 11");
	    permute(letters.length());
	}
  
	
	public static void permute(int k) {
		permute(k, new HashSet<Integer>(), new int[k]);
	}

	
	public static void permute(int k, HashSet<Integer> set, int[] permutation) {
		if (set.size() == k)
			doPermute(k, set, permutation);
		else {
			for (int i = 0; i < 10; i++)
				if (!set.contains(i)) {
					permutation[set.size()] = i;
					set.add(i);
					permute(k,set,permutation);
					set.remove(i);
					if (i == 9 && permutation[0] == 9)  // The final possible combination has been reached
						System.out.println("Total number of answers: " + answers);			
				}
		}
	}
  
	
	public static void doPermute(int k, HashSet<Integer> set, int[] permutation) {
		HashMap<Character,Integer> charmap = new HashMap<Character,Integer>();
		for (int i = 0; i < k; i++)
			charmap.put(letters.charAt(i), // "SENDMORY";
                  permutation[i]);   // [2, 3, 1, 4, 0, 5,... ]
		long[] vals = new long[3];
		for (int j = 0; j < 3; j++) {
			String word = words[j];
			if (charmap.get(word.charAt(0)) == 0) { // A number can't begin with a zero
				return;
				}
			long val = 0;
			for (int i = 0; i < word.length(); i++)
				val = 10*val + charmap.get(word.charAt(i));
			vals[j] = val;
		}
		if (vals[0] + vals[1] == vals[2]) {
			answers++;
			System.out.println(charmap);
			System.out.format("%8d\n+%7d\n=%7d\n",vals[0],vals[1],vals[2]);
		}
	}
}
