import java.util.LinkedList;

public class DoubleStack {

	private LinkedList<Double> elements;

	public static void main(String[] argum) {
		System.out.println(DoubleStack.interpret("4. 2. /"));
		System.out.println(DoubleStack.interpret("15. 12. -"));
		System.out.println(DoubleStack.interpret("3. 1. +"));
		System.out.println(DoubleStack.interpret("2. 3. *"));
	}

	DoubleStack() {
		elements = new LinkedList<>();
	}
	

	@Override
	public Object clone() throws CloneNotSupportedException {
		DoubleStack copy = new DoubleStack();
		for (int i = 0; i < elements.size(); i++) {
			copy.push(elements.get(i));
		}
		return copy;
	}

	public boolean stEmpty() {
		return elements.isEmpty();
	}

	public void push(double a) {
		 elements.addLast(a);
	}

	public double pop() {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException ("magasini alataitumine");
		}
		return elements.removeLast();
	}

	// aritmeetikatehe s ( + - * / ) magasini kahe pealmise elemendi vahel (tulemus pannakse uueks tipuks)
	public void op(String s) {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException("magasini alataitumine");
		}

		double x = pop();
		double y = pop();

		switch (s) {
        case "+":
           push(x + y);
           break;
        case "-":
           push(y - x);
           break;
        case "*":
           push(x * y);
           break;
        case "/":
           push(y / x);
           break;
     }
		}
	
	
	public double tos() {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException ("magasini alataitumine");
		}
		return elements.getLast();
	}
	
	@Override
	public boolean equals(Object o) {
		return ((DoubleStack)o).elements.equals(elements);
	}

	// teisendus s�neks (tipp l�pus)
	@Override
	public String toString() {
		if (stEmpty()) {
			return "tyhi";
		}
		return elements.toString();

	}
		
	public static double interpret(String pol) {
		if (!pol.contains("+") && !pol.contains("-") && !pol.contains("*") && !pol.contains("/")){
			throw new IllegalArgumentException("Puuduvad matemaatiliste tehete m�rgid");
		}
		if (!pol.matches(".*\\d+.*")) {
			throw new IllegalArgumentException("Puuduvad numbrid");
		}
		
		String[] jupid = pol.trim().split(" ");
		int nrCnt = 0;
		DoubleStack tmp = new DoubleStack();
		for (int i = 0; i < jupid.length; i++) {
			if (isDouble(jupid[i])) {
				tmp.push(Double.parseDouble(jupid[i]));
				nrCnt++;
			} else {
				tmp.op(jupid[i]);
			}
		}
		if (nrCnt == (jupid.length - nrCnt) + 1) {
			return tmp.tos();
		}
		throw new RuntimeException();
	}

	public static boolean isDouble(String txt) {
		try {
			Double.parseDouble(txt);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	//kasutatud: https://pastebin.com/YKFtGbhc
}