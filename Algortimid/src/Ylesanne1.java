public class Ylesanne1 {

   enum Color {red, green, blue};
   
//   public static void main (String[] param) {
//	   Color[] pallid = {Color.red, Color.blue, Color.green, Color.green, Color.blue, Color.green, Color.red};
//	   reorder(pallid);
//	   for(int i = 0; i < pallid.length; i++){
//		   System.out.println(pallid[i]);
//	   }
//   }
   
   public static void reorder (Color[] balls) {
	   Color[] balls2 = new Color[balls.length];
	   int firstIndex = 0;
	   int lastIndex = balls.length - 1;
	   for (int i = 0; i < balls.length; i++) {
			if (balls[i] == Color.red) {
				balls2[firstIndex] = balls[i];
				firstIndex++;				
			}
			if (balls[i] == Color.blue) {
				balls2[lastIndex] = balls[i];
				lastIndex--;				
			}
		}
	   for (int i = firstIndex; i < lastIndex + 1; i++) {
		   balls2[i] = Color.green;
	   }
//	   for (int i = 0; i < balls.length; i++) {
//		   balls[i] = balls2[i];
//	   }
	   System.arraycopy(balls2, 0, balls, 0, balls.length);
   }
}

