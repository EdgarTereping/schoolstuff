
/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
	   Lfraction fraction = new Lfraction(-4, 75);
	   System.out.println(fraction.getNumerator());
   }

   private long n;
   private long d;
   
   

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
	   if (b == 0)
		   throw new IllegalArgumentException("The denominator must not be zero");
       this.n = a;
       this.d = b;
       this.reduce();
   }
   
   public Lfraction(long a) {
       this.n = a;
       this.d = 1;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return n; 
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return d; 
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return n + "/" + d; 
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return n == ((Lfraction) m).n && d == ((Lfraction) m).d; 
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   //Eclipse generated hashcode
   public int hashCode() {
   	final int prime = 31;
   	int result = 1;
   	result = prime * result + (int) (d ^ (d >>> 32));
   	result = prime * result + (int) (n ^ (n >>> 32));
   	return result;
   }
   

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
	   long newNumerator = n * m.d + m.n * d;
	   long newDenominator = d * m.d;
       return new Lfraction(newNumerator, newDenominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
	   long newNumerator = n * m.n;
       long newDenominator = d * m.d;
       return new Lfraction(newNumerator, newDenominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
	   if (n == 0)
           throw new RuntimeException("The nominator is zero. If inverted the denominator will be zero");
       return new Lfraction(d, n);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
	   return new Lfraction(-n, d);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
	   long newNumerator = n * m.d - d * m.n;
       long newDenominator = d * m.d;
       return new Lfraction(newNumerator, newDenominator);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
	   if (m.n == 0)
           throw new IllegalArgumentException("The nominator of the divisor is zero. Can't divide by zero");
	   long newNumerator = n * m.d;
       long newDenominator = d * m.n;
       return new Lfraction(newNumerator, newDenominator);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
	   if (n * d < m.n * m.d)
           return -1;
       if (this.equals(m))
           return 0;
       return 1;   
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
	   return new Lfraction(this.getNumerator(), this.getDenominator());
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
	   double a = n / d;
       return (long) a;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
	   return this.minus(new Lfraction(this.integerPart()));
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
	   return (double) n / d;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
	  if (d <= 0)
		   throw new IllegalArgumentException("The denominator must be a positive number and not zero");
	  long a = Math.round(d * f);
	  return new Lfraction(a, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
	   String[] parts = s.split("/");
       if (parts.length != 2 || !isLong(parts[0]) || !isLong(parts[1])) {
           throw new RuntimeException("Invalid string: " + s);
       }
       long n = Long.parseLong(parts[0]);
       long d = Long.parseLong(parts[1]);
       return new Lfraction(n, d);
   }
   
   private static boolean isLong(String s) {
       try {
           Long.parseLong(s);
           return true;
       } catch (Exception e) {
           return false;
       }
   }
   //https://stackoverflow.com/questions/4009198/java-get-greatest-common-divisor
   public static long gcd(long a, long b) {
	   if (b==0) return a;
	   return gcd(b,a%b);
	}

	private void reduce() {
		long a = gcd(n, d);
		n /= a;
		d /= a;
		if (d < 0) {
            n *= -1;
            d *= -1;
        }
	}

}