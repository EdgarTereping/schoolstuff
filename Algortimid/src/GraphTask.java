
import java.util.*;

/**
 * @author 		Edgar Tereping
 * @version 	1.0 28 November 2017
 * Provided source code has been updated with necessary classes and methods to
 * find a set of vertices within 4 step reach from a given vertex.
 */

/** 
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (240, 241);
      Vertex v = g.first;
      List<Arc> narcs = g.getArcs();
      System.out.println(Arrays.toString(narcs.toArray()));      
      DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(g);
      List<Vertex> test = dijkstra.execute(v);
      System.out.println(Arrays.toString(test.toArray()));
      System.out.println(test.size());
   }

   /** Vertex represents a dot in the graph. It is connected by arcs. */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      /** 
       * Constructor for vertex
       * @param s vertex id
       * @param v next vertex
       * @param e arc connecting to the next vertex
       */
      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }
      
      /**
       * Constructor for vertex
       * @param s vertex id
       */
      Vertex (String s) {
         this (s, null, null);
      }
      
      /** 
       * Converts vertex to string
       * @return Vertex id
       */
      @Override
      public String toString() {
         return id;
      }
   }


   /** 
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      private Vertex source; // Added source vertex
      private int weight = 1; // Added vertex weight necessary for calculating distances between vertices

      /** 
       * Constructor for arc
       * @param s arc id
       * @param v target vertex
       * @param a next arc
       * @param so source vertex
       */
      Arc (String s, Vertex v, Arc a, Vertex so) {
         id = s;
         target = v;
         next = a;
         source = so; // Added source vertex
      }

      /**
       * Constructor for arc
       * @param s arc id
       */
      Arc (String s) {
         this (s, null, null, null);
      }
      
      /**
       * Converts arc to string
       * @return Arc id
       */
      @Override
      public String toString() {
         return id;
      }
      
      /**
       * Returns the arc's source vertex.
       * @return Source vertex
       */
      public Vertex getSource() { // Added getSource method
          return source;
      }
      
      /**
       * Returns the arc's target vertex.
       * @return Target vertex
       */    
      public Vertex getTarget() { // Added getTarget method
          return target;
      }
      
      /**
       * Returns the arc's weight.
       * @return Arc's weight
       */
      public int getWeight() { // Added getWeight method
          return weight;
      }
   }
   
   /** Graph represents a tree-like structure made of vertices and arcs.*/
   class Graph {
      private String id;
      private Vertex first;
      private int info = 0;

      /**
       * Constructor for graph
       * @param s graph id
       * @param v first vertex       
       */
      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }
      
      /**
       * Constructor for graph
       * @param Graph id      
       */
      Graph (String s) {
         this (s, null);
      }

      /**
       * Converts graph to string
       * @return Graph in string form
       */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }
      
      /**
       * Creates a vertex
       * @param vid string id
       * @return Result vertex
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Creates an arc
       * @param aid arc id
       * @param from source vertex
       * @param to target vertex
       * @return Result arc
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         res.source = from; // Added source vertex
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }         
         
      }
      
      /**
       * Creates a list of all vertices.
       * @return List of all vertices
       */
      public List<Vertex> getVertices() { // Added getVertices method
    	  List<Vertex> vertices = new ArrayList<>();
    	  Vertex v = first;
          while (v != null) {
    	     vertices.add(v);
    	     v = v.next;
          }
    	  return vertices;
      }
         
      /**
       * Creates a list of all arcs.
       * @return List of all arcs
       */
      public List<Arc> getArcs() {  // Added getArcs method
    	  List<Arc> arcs = new ArrayList<>();
    	  Vertex v = first;
    	  while (v != null) {
              Arc a = v.first;    
              while (a != null) {
            	  arcs.add(a);
                  a = a.next;              
                  }
              v = v.next;
          }    	  
          return arcs;
      }
      
  	}
   
   /** 
    * Class of the Dijkstra algorithm containing methods to
    * find a set of vertices within 4 step reach from a given vertex.
    * Source: http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html
    */
   public class DijkstraAlgorithm {

 	    private List<Vertex> vertices;
 	    private List<Arc> arcs;
 	    private Set<Vertex> settledNodes;
 	    private Set<Vertex> unSettledNodes;
 	    private Map<Vertex, Vertex> predecessors;
 	    private Map<Vertex, Integer> distance;
 	    
 	   /** 
 	    * Creates a copy of the array so we can operate on it
 	    * @param graph Graph
 	    */
 	    public DijkstraAlgorithm(Graph graph) {
 	        this.vertices = graph.getVertices();
 	        this.arcs = graph.getArcs();
 	    }

 	   /** 
  	    * Dijkstra algorithm's modified main method which returns the reachable vertices
  	    * @param source Source vertex
  	    * @return List of vertices within 4 hops from the source vertex
  	    */
 	    public List<Vertex> execute(Vertex source) {
 	    	List<Vertex> reachableVertices = new ArrayList<>(); // Added a list of reachable vertices
 	        settledNodes = new HashSet<Vertex>();
 	        unSettledNodes = new HashSet<Vertex>();
 	        distance = new HashMap<Vertex, Integer>();
 	        predecessors = new HashMap<Vertex, Vertex>();
 	        distance.put(source, 0);
 	        unSettledNodes.add(source);
 	        while (unSettledNodes.size() > 0) {
 	        	System.out.println(Arrays.asList(distance));
 	            Vertex node = getMinimum(unSettledNodes);
 	            settledNodes.add(node);
 	            unSettledNodes.remove(node);
 	            findMinimalDistances(node);
 	            if (distance.get(node) > 4) // Modified algorithm to return reachable vertices within 4 hops
                   break;
 	            reachableVertices.add(node);
 	                	
 	        }
        	return reachableVertices;
 	    }

 	   /** 
   	    * Finds the minimal distances between vertex and adjacent vertices
   	    * @param node Vertex
   	    */
 	   private void findMinimalDistances(Vertex node) {
 	        List<Vertex> adjacentNodes = getNeighbors(node);
 	        for (Vertex target : adjacentNodes) {
 	            if (getShortestDistance(target) > getShortestDistance(node)
 	                    + getDistance(node, target)) {
 	                distance.put(target, getShortestDistance(node)
 	                        + getDistance(node, target));
 	                predecessors.put(target, node);
 	                unSettledNodes.add(target);
 	            }
 	        }

 	    }
 	    
 	  /** 
   	    * Finds the distance between two vertices
   	    * @param node Source vertex
   	    * @param target Target vertex 
   	    * @return Distance of vertex from source vertex
   	    */
 	   private int getDistance(Vertex node, Vertex target) {
 	        for (Arc arc : arcs) {
 	            if (arc.getSource().equals(node)
 	                    && arc.getTarget().equals(target)) {
 	                return arc.getWeight();
 	            }
 	        }
 	        throw new RuntimeException("Should not happen");
 	    }

 	  /** 
   	    * Finds the neighbor vertices of a vertex
   	    * @param node Vertex
   	    * @return Neighbor vertices
   	    */
 	    private List<Vertex> getNeighbors(Vertex node) {
 	        List<Vertex> neighbors = new ArrayList<Vertex>();
 	        for (Arc arc : arcs) {
 	            if (arc.getSource().equals(node)
 	                    && !isSettled(arc.getTarget())) {
 	                neighbors.add(arc.getTarget());
 	            }
 	        }
 	        return neighbors;
 	    }

 	   /** 
    	 * Finds the vertex with the minimum distance
    	 * @param vertexes Set of vertices
    	 * @return Vertex with the minimum distance 
    	 */
 	    private Vertex getMinimum(Set<Vertex> vertexes) {
 	        Vertex minimum = null;
 	        for (Vertex vertex : vertexes) {
 	            if (minimum == null) {
 	                minimum = vertex;
 	            } else {
 	                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
 	                    minimum = vertex;
 	                }
 	            }
 	        }
 	        return minimum;
 	    }

 	   /** 
    	 * Checks if vertex is settled
    	 * @param vertex Vertex
    	 * @return Boolean value to show if the vertex is settled
    	 */
 	    private boolean isSettled(Vertex vertex) {
 	        return settledNodes.contains(vertex);
 	    }

 	   /** 
    	 * Helper method for setting distance value
    	 * @param destination Destination vertex
    	 * @return Distance value
    	 */
 	    private int getShortestDistance(Vertex destination) {
 	        Integer d = distance.get(destination);
 	        if (d == null) {
 	            return Integer.MAX_VALUE;
 	        } else {
 	            return d;
 	        }
 	    }
   }
} 
