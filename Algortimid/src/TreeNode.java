
import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;
   public static boolean alreadyExecuted = false;

   public TreeNode (String n, TreeNode d, TreeNode r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }
   
   public TreeNode () {

	   }   
   
   public static void IsStringValid(String s) {
	   if (s.contains(" ") || s.contains(",,") || s.contains("(,"))
		   throw new RuntimeException("Invalid string, please check: " + s);
	   if (s.contains("(") && s.charAt(s.length() - 1) != ')')
		   throw new RuntimeException("Invalid string: " + s);
	   if (s.contains("()"))
		   throw new RuntimeException("Tree " + s + "contains an empty subtree");
	   int counter1 = 0, counter2 = 0;	
	   for (int i = 0; i < s.length(); i++) {		   	   
		   if (s.charAt(i) == '(')
			   counter1 += 1;
		   if (s.charAt(i) == ')') {
			   counter2 += 1;
			   if (counter1 == counter2 && i < s.length() - 1)
				   throw new RuntimeException("Invalid string: " + s);
		   }
	   }
	   if (counter1 != counter2)
		   throw new RuntimeException("Uneven amount of brackets in string: " + s);
	   if (s.contains(",") && !s.contains("("))
		   throw new RuntimeException("Invalid string: " + s);
	   char c = s.charAt(0);
	   if (!Character.isLetterOrDigit(c) && c != '*' && c != '/' && c != '+' && c != '-')
		   throw new RuntimeException("Invalid string: " + s);
	   alreadyExecuted = true;
   }
   
   public static TreeNode parsePrefix (String s) {
	   if (!alreadyExecuted)
		   IsStringValid(s);
	   if (s == null || s.isEmpty() )
		   return null;
	   StringTokenizer st = new StringTokenizer(s,",()",true);
	   String root = st.nextToken();	  
	   String helper = "", fChild = "", nSibling = "";
	   while(st.hasMoreTokens())	{
		   helper = st.nextToken();		   
		   if(helper.equals("(")) {	
			   helper = st.nextToken();
			   if (helper.equals("("))
				   throw new RuntimeException("Invalid string: " + s);
			   int brackets = 0; 
			   while(brackets < 1) {
				   fChild = fChild + helper;	//firstChild
				   helper = st.nextToken();
				   if(helper.equals("("))
					   brackets = brackets - 1;
				   else if(helper.equals(")"))
					   brackets = brackets + 1;
			   }
		   }
		   if (helper.equals(",")) 
			   while(st.hasMoreTokens()) 
				   nSibling = nSibling + st.nextToken(); //nextSibling		 	   
	   }
	   return new TreeNode(root,TreeNode.parsePrefix(fChild),TreeNode.parsePrefix(nSibling));
   }

   public String rightParentheticRepresentation() {
      StringBuilder result = new StringBuilder();
      if (firstChild != null) {
    	  result.append('(');
    	  result.append(firstChild.rightParentheticRepresentation());
    	  result.append(')');
      }
    	  result.append(name);       
      if (nextSibling != null) {
    	  result.append(',');
    	  result.append(nextSibling.rightParentheticRepresentation());
      }
      alreadyExecuted = false;
      return result.toString();
   }

   public static void main (String[] param) {
      String s = "A(B,,C)";
      String s2 = "A(B,,C)";
      TreeNode t = TreeNode.parsePrefix (s);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}

//https://gist.github.com/not-much-io/01c6abfd14a9df1fefb7